package it.urial.de_cifris.view.table;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Employee {
	
	SimpleIntegerProperty id;
	SimpleStringProperty name;
	SimpleStringProperty surname;
	SimpleStringProperty fiscal_code;
	
	public Employee(Integer id, String name, String surname,
			String fiscal_code) {
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.surname = new SimpleStringProperty(surname);
		this.fiscal_code = new SimpleStringProperty(fiscal_code);
	}
	
	
	public SimpleIntegerProperty getId() {
		return id;
	}
	public SimpleStringProperty getName() {
		return name;
	}
	public SimpleStringProperty getSurname() {
		return surname;
	}
	public SimpleStringProperty getFiscal_code() {
		return fiscal_code;
	}
	
	
}