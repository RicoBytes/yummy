package it.urial.de_cifris.view;

import java.io.IOException;

import it.urial.de_cifris.control.file.FileManager;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SetupStage extends Stage {
	
	private boolean firstTime = true;
	
	public SetupStage() throws IOException {
		AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/setup_b-1.fxml"));
		this.getIcons().add(new Image(this.getClass().getResourceAsStream("/logo.png")));
		this.setScene(new Scene(pane, 600, 400));
		this.setTitle("Yummy! Database Setup");
		this.setResizable(false);
		this.initModality(Modality.APPLICATION_MODAL);
		
		this.setOnCloseRequest(e -> {
			if(firstTime) {
				Platform.exit();
			}
		});
	}
	
	public boolean requiresSetup() {
		boolean result = false;
		try {
			firstTime = Boolean.parseBoolean(FileManager.getConfigurationFile().getProperties().getProperty("setup"));
			result = Boolean.parseBoolean(FileManager.getConfigurationFile().getProperties().getProperty("setup"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
