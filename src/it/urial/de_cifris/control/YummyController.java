package it.urial.de_cifris.control;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import it.urial.de_cifris.control.file.FileManager;
import it.urial.de_cifris.home.Yummy;
import it.urial.de_cifris.model.FileConfiguration;
import it.urial.de_cifris.view.Category;
import it.urial.de_cifris.view.table.Employee;
import it.urial.de_cifris.view.table.Product;
import it.urial.de_cifris.view.table.Sale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class YummyController implements Initializable {
	
	/*
	 * ROOTS
	 */
	
	@FXML
	private Pane homePane;
	@FXML
	private Pane mainPane;
	
	/*
	 * MENU
	 */
	
	@FXML
	private Pane homeMenuPane;
	@FXML
	private Pane productsMenuPane;
	@FXML
	private Pane salesMenuPane;
	@FXML
	private Pane employeesMenuPane;
	@FXML
	private Pane settingsMenuPane;
	
	/*
	 * HOME
	 */
	
	@FXML
	private JFXButton productsButton;
	@FXML
	private JFXButton salesButton;
	
	@FXML
	private Label runsetupLabel;
	
	/*
	 * MAIN PANE
	 */
	
	@FXML
	private Label titleLabel;
	@FXML
	private Label subtitleLabel;
	
	
	@FXML
	private JFXButton parametersButton;
	@FXML
	private JFXButton closeParametersButton;
	@FXML
	private JFXButton searchButton;
	@FXML
	private JFXTextField searchTextField;
	@FXML
	private AnchorPane parametersPane;
	@FXML
	private JFXComboBox<String> typeParameterComboBox;
	@FXML
	private JFXDatePicker fromDatePicker;
	@FXML
	private JFXDatePicker toDatePicker;

	private HashMap<Integer, String> cacheParams = new HashMap<Integer, String>();
	
	@FXML
	private JFXButton addDataButton;
	@FXML
	private JFXButton removeDataButton;
	
	@FXML
	private JFXButton updateTableButton;
	
	//PRODUCTS
	@FXML
	private TreeTableView<Product> productsTreeTableView;
	
	@FXML
	private TreeTableColumn<Product, Number> idProductsColumn;
	@FXML
	private TreeTableColumn<Product, String> nameProductsColumn;
	@FXML
	private TreeTableColumn<Product, Number> amountProductsColumn;
	@FXML
	private TreeTableColumn<Product, Number> priceProductsColumn;
	@FXML
	private TreeTableColumn<Product, String> descriptionProductsColumn;
	@FXML
	private TreeTableColumn<Product, String> typeProductsColumn;
	@FXML
	private TreeTableColumn<Product, Number> weightProductsColumn;
	@FXML
	private TreeTableColumn<Product, String> provenanceProductsColumn;
	
	//PRODUCTS
	@FXML
	private TreeTableView<Sale> salesTreeTableView;
	
	@FXML
	private TreeTableColumn<Sale, Number> idSalesColumn;
	@FXML
	private TreeTableColumn<Sale, Number> idProductSalesColumn;
	@FXML
	private TreeTableColumn<Sale, Number> idEmployeeSalesColumn;
	@FXML
	private TreeTableColumn<Sale, String> dateSalesColumn;
	
	//Employee
	@FXML
	private TreeTableView<Employee> employeesTreeTableView;
	
	@FXML
	private TreeTableColumn<Employee, Number> idEmployeeColumn;
	@FXML
	private TreeTableColumn<Employee, String> nameEmployeeColumn;
	@FXML
	private TreeTableColumn<Employee, String> surnameEmployeeColumn;
	@FXML
	private TreeTableColumn<Employee, String> fiscalcodeEmployeeColumn;
	
	/*
	 * STATUS
	 */
	
	@FXML
	private Label statusLabel;
	@FXML
	private Circle statusCircle;
	
	/*
	 * LOGOS
	 */
	@FXML
	private ImageView logoImageView;
	@FXML
	private ImageView productsImageView;
	@FXML
	private ImageView salesImageView;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logoImageView.setImage(new Image(this.getClass().getResourceAsStream("/logo.png")));
		productsImageView.setImage(new Image(this.getClass().getResourceAsStream("/cart.png")));
		salesImageView.setImage(new Image(this.getClass().getResourceAsStream("/box.png")));
		
		try {
			if(Yummy.getMySQLInstance() != null) {
				if(Yummy.getMySQLInstance().getConnection() != null) {
					if(!Yummy.getMySQLInstance().getConnection().isClosed()) {
						statusLabel.setText("Connesso");
						statusCircle.setFill(Paint.valueOf("Green"));
					} else {
						try {
							FileConfiguration fc = FileManager.getConfigurationFile();
							fc.read();
							fc.write("setup", "true");
							fc.apply();
						} catch (IOException e1) { }
					}
				} else {
					try {
						FileConfiguration fc = FileManager.getConfigurationFile();
						fc.read();
						fc.write("setup", "true");
						fc.apply();
					} catch (IOException e1) { }
				}
			} else {
				try {
					FileConfiguration fc = FileManager.getConfigurationFile();
					fc.read();
					fc.write("setup", "true");
					fc.apply();
				} catch (IOException e1) { }
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/*
		 * HOME (Eventi)
		 */
		
		runsetupLabel.setOnMouseClicked(e -> {
			StageManager.getSetupStage().show();
		});
		
		/*
		 * INTERAZIONE PANNELLI PRINCIPALI (Eventi)
		 */
		
		searchButton.setOnMouseClicked(e -> {
			updateTables();
		});
		
		parametersButton.setOnMouseClicked(e -> {
			parametersPane.setVisible(!parametersPane.isVisible());

			List<String> list = new ArrayList<>();
			
			switch(StageManager.getPrimaryStage().getCategory()) {
			case EMPLOYEES:
				try {
					Yummy.getMySQLInstance().getColumns("dipendente").forEach(k->{
						cacheParams.put(list.size(), k);
						list.add(k);
					});
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				break;
			case HOME:
				break;
			case PRODUCTS:
				try {
					Yummy.getMySQLInstance().getColumns("prodotto").forEach(k->{
						cacheParams.put(list.size(), k);
						list.add(k);
					});
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				break;
			case SALES:
				try {
					Yummy.getMySQLInstance().getColumns("vende").forEach(k->{
						cacheParams.put(list.size(), k);
						list.add(k);
					});
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				break;
			
			}
			ObservableList<String> obslist = FXCollections.observableList(list);
			typeParameterComboBox.getItems().setAll(obslist);
			cacheParams.clear();
		});
		
		closeParametersButton.setOnMouseClicked(e -> {
			parametersPane.setVisible(false);
		});
		
		
		addDataButton.setOnMouseClicked(e -> {
			StageManager.getDtoStage().show();
		});
		
		removeDataButton.setOnMouseClicked(e -> {
			switch(StageManager.getPrimaryStage().getCategory()) {
			case EMPLOYEES:
				if(employeesTreeTableView.getSelectionModel().getSelectedItem() != null) {
					Employee empl = employeesTreeTableView.getSelectionModel().getSelectedItem().getValue();
					
					try {
						Yummy.getMySQLInstance().execute("DELETE FROM dipendente WHERE id_dipendente='" + empl.getId().getValue() + "';");
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
				}
				break;
			case HOME:
				break;
			case PRODUCTS:
				if(productsTreeTableView.getSelectionModel().getSelectedItem() != null) {
					Product prod = productsTreeTableView.getSelectionModel().getSelectedItem().getValue();
					
					try {
						Yummy.getMySQLInstance().execute("DELETE FROM prodotto WHERE id_prodotto='" + prod.getId().getValue() + "';");
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
				}
				break;
			case SALES:
				if(salesTreeTableView.getSelectionModel().getSelectedItem() != null) {
					Sale sale = salesTreeTableView.getSelectionModel().getSelectedItem().getValue();
					
					try {
						Yummy.getMySQLInstance().execute("DELETE FROM vende WHERE id_vendita='" + sale.getId_sale().getValue() + "';");
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
				}
				break;
			default:
				break;
			}
			
			updateTables();
		});
		
		/*
		 * INTERAZIONI DEL MENU' (Eventi)
		 */
		
		homeMenuPane.setOnMouseClicked(e ->{
			homePane.setVisible(true);
			mainPane.setVisible(false);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			homeMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			productsMenuPane.setStyle("-fx-background-color: #29b6f6");
			salesMenuPane.setStyle("-fx-background-color: #29b6f6");
			employeesMenuPane.setStyle("-fx-background-color: #29b6f6");
		});
		
		
		productsMenuPane.setOnMouseClicked(e ->{
			StageManager.getPrimaryStage().setCurrentCategory(Category.PRODUCTS);
			
			homeMenuPane.setStyle("-fx-background-color: #29b6f6");
			productsMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			salesMenuPane.setStyle("-fx-background-color: #29b6f6");
			employeesMenuPane.setStyle("-fx-background-color: #29b6f6");
			
			homePane.setVisible(false);
			mainPane.setVisible(true);
			productsTreeTableView.setVisible(true);
			salesTreeTableView.setVisible(false);
			employeesTreeTableView.setVisible(false);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			titleLabel.setText("Prodotti");
			
			updateTables();
			
		});
		salesMenuPane.setOnMouseClicked(e->{
			StageManager.getPrimaryStage().setCurrentCategory(Category.SALES);
			
			homeMenuPane.setStyle("-fx-background-color: #29b6f6");
			productsMenuPane.setStyle("-fx-background-color: #29b6f6");
			salesMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			employeesMenuPane.setStyle("-fx-background-color: #29b6f6");
			
			homePane.setVisible(false);
			mainPane.setVisible(true);
			productsTreeTableView.setVisible(false);
			salesTreeTableView.setVisible(true);
			employeesTreeTableView.setVisible(false);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			titleLabel.setText("Vendite");
			
			updateTables();
		});
		employeesMenuPane.setOnMouseClicked(e->{
			StageManager.getPrimaryStage().setCurrentCategory(Category.EMPLOYEES);
			
			homeMenuPane.setStyle("-fx-background-color: #29b6f6");
			productsMenuPane.setStyle("-fx-background-color: #29b6f6");
			salesMenuPane.setStyle("-fx-background-color: #29b6f6");
			employeesMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			
			homePane.setVisible(false);
			mainPane.setVisible(true);
			
			productsTreeTableView.setVisible(false);
			salesTreeTableView.setVisible(false);
			employeesTreeTableView.setVisible(true);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			titleLabel.setText("Dipendenti");
			
			updateTables();
		});
		
		productsButton.setOnMouseClicked(e->{
			StageManager.getPrimaryStage().setCurrentCategory(Category.PRODUCTS);
			
			homeMenuPane.setStyle("-fx-background-color: #29b6f6");
			productsMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			salesMenuPane.setStyle("-fx-background-color: #29b6f6");
			employeesMenuPane.setStyle("-fx-background-color: #29b6f6");
			
			homePane.setVisible(false);
			mainPane.setVisible(true);
			
			productsTreeTableView.setVisible(true);
			salesTreeTableView.setVisible(false);
			employeesTreeTableView.setVisible(false);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			titleLabel.setText("Prodotti");
			
			updateTables();
		});
		salesButton.setOnMouseClicked(e->{
			StageManager.getPrimaryStage().setCurrentCategory(Category.SALES);
			
			homeMenuPane.setStyle("-fx-background-color: #29b6f6");
			productsMenuPane.setStyle("-fx-background-color: #29b6f6");
			salesMenuPane.setStyle("-fx-background-color: linear-gradient(to right, #0086c3 10%,  #29b6f6)");
			employeesMenuPane.setStyle("-fx-background-color: #29b6f6");
			
			homePane.setVisible(false);
			mainPane.setVisible(true);
			
			productsTreeTableView.setVisible(false);
			salesTreeTableView.setVisible(true);
			employeesTreeTableView.setVisible(false);
			
			parametersPane.setVisible(false);
			typeParameterComboBox.getSelectionModel().clearSelection();
			searchTextField.setText("");
			
			titleLabel.setText("Vendite");
			
			updateTables();
		});
		
		updateTableButton.setOnMouseClicked(e -> {
			updateTables();
		});
		
	}
	
	/**
	 * Aggiorna le tabelle, in base alla categoria selezionata dal men�.
	 */
	private void updateTables() {
		switch(StageManager.getPrimaryStage().getCategory()) {
		case EMPLOYEES:
			TreeItem<Employee> root = new TreeItem<>(new Employee(0, "", "", ""));
			List<TreeItem<Employee>> list = new ArrayList<TreeItem<Employee>>();
			ObservableList<TreeItem<Employee>> observableList = FXCollections.observableList(list);
			
			try {
				Yummy.getMySQLInstance().getEmployees(typeParameterComboBox.getSelectionModel().getSelectedItem(), searchTextField.getText()).forEach(p -> {
					observableList.add(new TreeItem<Employee>(p));
				});
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			root.getChildren().setAll(observableList);
			
			idEmployeeColumn.setCellValueFactory(param -> param.getValue().getValue().getId());
			nameEmployeeColumn.setCellValueFactory(param -> param.getValue().getValue().getName());
			surnameEmployeeColumn.setCellValueFactory(param -> param.getValue().getValue().getSurname());
			fiscalcodeEmployeeColumn.setCellValueFactory(param -> param.getValue().getValue().getFiscal_code());
			
			employeesTreeTableView.setRoot(root);
			employeesTreeTableView.setShowRoot(false);
			break;
		case HOME:
			break;
		case PRODUCTS:
			TreeItem<Product> root1 = new TreeItem<>(new Product(0, "", 0, 0, "", "", 0, ""));
			List<TreeItem<Product>> list1 = new ArrayList<TreeItem<Product>>();
			ObservableList<TreeItem<Product>> observableList1 = FXCollections.observableList(list1);
			//TODO
			try {
				Yummy.getMySQLInstance().getProducts(typeParameterComboBox.getSelectionModel().getSelectedItem(), searchTextField.getText()).forEach(p -> {
					observableList1.add(new TreeItem<Product>(p));
				});
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			root1.getChildren().setAll(observableList1);
			
			idProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getId());
			nameProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getName());
			amountProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getAmount());
			priceProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getPrice());
			descriptionProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getDescription());
			typeProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getType());
			weightProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getWeight());
			provenanceProductsColumn.setCellValueFactory(param -> param.getValue().getValue().getProvenance());
			
			productsTreeTableView.setRoot(root1);
			productsTreeTableView.setShowRoot(false);
			break;
		case SALES:
			TreeItem<Sale> root2 = new TreeItem<>(new Sale(0, 0, 0, ""));
			List<TreeItem<Sale>> list2 = new ArrayList<TreeItem<Sale>>();
			ObservableList<TreeItem<Sale>> observableList2 = FXCollections.observableList(list2);
			
			try {
				Yummy.getMySQLInstance().getSales(typeParameterComboBox.getSelectionModel().getSelectedItem(), searchTextField.getText()).forEach(p -> {
					observableList2.add(new TreeItem<Sale>(p));
				});
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			root2.getChildren().setAll(observableList2);
			
			idSalesColumn.setCellValueFactory(param -> param.getValue().getValue().getId_sale());
			idProductSalesColumn.setCellValueFactory(param -> param.getValue().getValue().getId_product());
			idEmployeeSalesColumn.setCellValueFactory(param -> param.getValue().getValue().getId_employee());
			dateSalesColumn.setCellValueFactory(param -> param.getValue().getValue().getDate());
			
			salesTreeTableView.setRoot(root2);
			salesTreeTableView.setShowRoot(false);
			break;
		
		}
	}
	
}
