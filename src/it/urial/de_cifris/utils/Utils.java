package it.urial.de_cifris.utils;

public class Utils {
	
	/**
	 * 
	 * @param text
	 * @return Controlla se il testo contiene solamente numeri
	 */
	public static boolean isNumber(String text) {
		boolean result = false;
		
		try {
			Integer.parseInt(text);
			result = true;
			
		} catch(Exception e) { }
		
		return result;
	}
	
}
