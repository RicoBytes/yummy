package it.urial.de_cifris.control.file;

import java.io.File;
import java.io.IOException;

import it.urial.de_cifris.model.FileConfiguration;

public class FileManager {
	
	public static final File ROOT = new File(System.getProperty("user.home"), "Yummy");
	public static final File YUMMY_INI = new File(ROOT.getAbsolutePath(), "yummy.ini");
	
	/**
	 * Crea la cartella principale, ed il file che ci servir� in futuro per configurare i dati del database
	 * @throws IOException
	 */
	public static void init() throws IOException {
		ROOT.mkdirs();
		
		if(!YUMMY_INI.exists()) YUMMY_INI.createNewFile();
		
		FileConfiguration fc = new FileConfiguration(YUMMY_INI);
		fc.read();
		if(!fc.exists("setup")) {
			fc.write("setup", "true");
		}
		
		fc.apply();
	}
	
	/**
	 * 
	 * @return Restituisce la cartella root
	 */
	public static File getRoot() {
		return ROOT;
	}
	
	/**
	 * 
	 * @return Restituisce il file principale
	 */
	public static File getFile() {
		return YUMMY_INI;
	}
	
	/**
	 * 
	 * @return Restituisce il FileConfiguration principale
	 */
	public static FileConfiguration getConfigurationFile() {
		return new FileConfiguration(YUMMY_INI);
	}
	
}
