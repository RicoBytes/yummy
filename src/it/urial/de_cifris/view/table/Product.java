package it.urial.de_cifris.view.table;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Product {
	
	SimpleIntegerProperty id;
	SimpleStringProperty name;
	SimpleIntegerProperty amount;
	SimpleIntegerProperty price;
	SimpleStringProperty description;
	SimpleStringProperty type;
	SimpleIntegerProperty weight;
	SimpleStringProperty provenance;
	
	public Product(Integer id, String name, Integer amount, Integer price,
			String description, String type, Integer weight, String provenance) {
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.amount = new SimpleIntegerProperty(amount);
		this.price = new SimpleIntegerProperty(price);
		this.description = new SimpleStringProperty(description);
		this.type = new SimpleStringProperty(type);
		this.weight = new SimpleIntegerProperty(weight);
		this.provenance = new SimpleStringProperty(provenance);
	}

	public SimpleIntegerProperty getId() {
		return id;
	}
	
	public SimpleStringProperty getName() {
		return name;
	}
	public SimpleIntegerProperty getAmount() {
		return amount;
	}
	public SimpleIntegerProperty getPrice() {
		return price;
	}
	public SimpleStringProperty getDescription() {
		return description;
	}
	public SimpleStringProperty getType() {
		return type;
	}
	public SimpleIntegerProperty getWeight() {
		return weight;
	}
	public SimpleStringProperty getProvenance() {
		return provenance;
	}
	
}
