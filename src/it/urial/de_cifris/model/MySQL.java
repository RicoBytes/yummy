package it.urial.de_cifris.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import it.urial.de_cifris.control.file.FileManager;
import it.urial.de_cifris.utils.Utils;
import it.urial.de_cifris.view.table.Employee;
import it.urial.de_cifris.view.table.Product;
import it.urial.de_cifris.view.table.Sale;

public class MySQL {
	
	private Connection connection;
	
	private String hostname, database, username, password;
	private int port;
	
	public MySQL(String hostname, String database, String username, String password, int port) {
		this.hostname = hostname;
		this.database = database;
		this.username = username;
		this.password = password;
		this.port = port;
	}
	
	/**
	 * Permette il login al database utilizzando i dati presenti nel file
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public MySQL() throws FileNotFoundException, IOException {
		FileConfiguration fc = FileManager.getConfigurationFile();
		fc.read();
		
		Properties prop = fc.getProperties();
		
		this.hostname = prop.getProperty("mysql.hostname");
		this.database = prop.getProperty("mysql.database");
		this.username = prop.getProperty("mysql.username");
		this.password = prop.getProperty("mysql.password");
		this.port = Utils.isNumber(prop.getProperty("mysql.port")) ? Integer.valueOf(prop.getProperty("mysql.port")) : 3306;
	}
	
	/**
	 * Stabilisce la connessione tra il software ed il database utilizzando la libreria MySQL Connector
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void establish() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.connection = DriverManager.getConnection("jdbc:mysql://" + hostname
				+ ":" + port
				+ "/" + database
				, username
				, password);
	}
	
	/**
	 * Esegue una query al database
	 * 
	 * @param query
	 * @throws SQLException
	 */
	public void execute(String query) throws SQLException {
		Statement stmt = this.getConnection().createStatement();
		
		stmt.executeUpdate(query);
		stmt.close();
	}
	
	/**
	 * Inserisce un prodotto alla tabella "prodotto"
	 * 
	 * @param name
	 * @param amount
	 * @param price
	 * @param description
	 * @param type
	 * @param weight
	 * @param provenance
	 * @throws SQLException
	 */
	public void insertProduct(String name, int amount, int price, String description, String type, int weight, String provenance) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement("INSERT INTO prodotto (nome, quantita, prezzo, descrizione, tipo, peso, provenienza) values (?, ?, ?, ?, ?, ?, ?);");
		
		stmt.setString(1, name);
		stmt.setInt(2, amount);
		stmt.setInt(3, price);
		stmt.setString(4, description);
		stmt.setString(5, type);
		stmt.setInt(6, weight);
		stmt.setString(7, provenance);
		
		stmt.execute();
		stmt.close();
	}
	
	/**
	 * Inserisce una vendita alla tabella "vendita"
	 * 
	 * @param id_product
	 * @param id_employee
	 * @param date
	 * @throws SQLException
	 */
	public void insertSale(int id_product, int id_employee, Date date) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement("INSERT INTO vende (id_prodotto, id_dipendente, data) values (?, ?, ?);");
		
		stmt.setInt(1, id_product);
		stmt.setInt(2, id_employee);
		stmt.setDate(3, date);
		
		stmt.execute();
		stmt.close();
	}

	/**
	 * Inserisce un dipendente alla tabella "dipendente"
	 * 
	 * @param name
	 * @param surname
	 * @param fiscal_code
	 * @throws SQLException
	 */
	public void insertEmployee(String name, String surname, String fiscal_code) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement("INSERT INTO dipendente (nome, cognome, codice_fiscale) values (?, ?, ?);");
		
		stmt.setString(1, name);
		stmt.setString(2, surname);
		stmt.setString(3, fiscal_code);
		
		stmt.execute();
		stmt.close();
	}
	
	/**
	 * Controlla se la query restituisce un risultato, cos� da ottenere un valore booleano, in base alla risposta.
	 * 
	 * @param query
	 * @return Returns the result of the query using boolean value
	 * @throws SQLException
	 */
	public boolean exists(String query) throws SQLException {
		boolean result = false;
		Statement stmt = this.connection.createStatement();
		
		ResultSet rs = stmt.executeQuery(query);
		if(rs.next()) {
			result = true;
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param column
	 * @param param
	 * @return Restituisce tutti i prodotti utilizzando dei parametri
	 * @throws SQLException
	 */
	public List<Product> getProducts(String column, String param) throws SQLException {
		List<Product> list = new ArrayList<>();
		
		PreparedStatement stmt = this.connection.prepareStatement(param.isEmpty() ? "select * from prodotto" : "select * from prodotto where " + column + "='" + param + "';");
		ResultSet rs = stmt.executeQuery();
		ResultSetMetaData rsmd=rs.getMetaData();
		
		while(rs.next()) {
			List<String> items = new ArrayList<String>();
			
			for(int i = 1; i < rsmd.getColumnCount()+1; i++) {
				items.add(rs.getString(i));
			}
			
			Product prod = new Product(Integer.parseInt(items.get(0)),
					items.get(1),
					Integer.parseInt(items.get(2)),
					Integer.parseInt(items.get(3)),
					items.get(4),
					items.get(5),
					Integer.parseInt(items.get(6)),
					items.get(7));
			
			
			list.add(prod);
		}
		
		return list;
	}
	
	/**
	 * 
	 * @param column
	 * @param param
	 * @return Restituisce tutte le vendite utilizzando dei parametri
	 * @throws SQLException
	 */
	public List<Sale> getSales(String column, String param) throws SQLException {
		List<Sale> list = new ArrayList<>();
		
		PreparedStatement stmt = this.connection.prepareStatement(param.isEmpty() ? "select * from vende" : "select * from vende where " + column + "='" + param + "';");
		ResultSet rs = stmt.executeQuery();
		ResultSetMetaData rsmd=rs.getMetaData();
		
		while(rs.next()) {
			List<String> items = new ArrayList<String>();
			
			for(int i = 1; i < rsmd.getColumnCount()+1; i++) {
				items.add(rs.getString(i));
			}
			
			Sale sale = new Sale(Integer.parseInt(items.get(0)),
					Integer.parseInt(items.get(1)),
					Integer.parseInt(items.get(2)),
					items.get(3));
			
			
			list.add(sale);
		}
		
		return list;
	}
	
	/**
	 * 
	 * @param column
	 * @param param
	 * @return Restituisce tutti gli impiegati utilizzando dei parametri
	 * @throws SQLException
	 */
	public List<Employee> getEmployees(String column, String param) throws SQLException {
		List<Employee> list = new ArrayList<>();
		
		PreparedStatement stmt = this.connection.prepareStatement(param.isEmpty() ? "select * from dipendente" : "select * from dipendente where " + column + "='" + param + "';");
		ResultSet rs = stmt.executeQuery();
		ResultSetMetaData rsmd=rs.getMetaData();
		
		while(rs.next()) {
			List<String> items = new ArrayList<String>();
			
			for(int i = 1; i < rsmd.getColumnCount()+1; i++) {
				items.add(rs.getString(i));
			}
			
			Employee empl = new Employee(Integer.parseInt(items.get(0)),
					items.get(1),
					items.get(2),
					items.get(3));
			
			
			list.add(empl);
		}
		
		return list;
	}
	
	/**
	 * 
	 * @param table
	 * @return Restituisce la lista delle colonne in una tabella
	 * @throws SQLException
	 */
	public List<String> getColumns(String table) throws SQLException {
		List<String> list = new ArrayList<>();
		
		PreparedStatement stmt = this.connection.prepareStatement("show columns from " + table + ";");
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			list.add(rs.getString(1));
		}
		
		return list;
	}
	
	/**
	 * 
	 * @return Restituisce la connessione MySQL
	 */
	public Connection getConnection() {
		return this.connection;
	}
	
	
	public String getHostname() {
		return hostname;
	}
	
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	public String getDatabase() {
		return database;
	}
	
	public void setDatabase(String database) {
		this.database = database;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
}
