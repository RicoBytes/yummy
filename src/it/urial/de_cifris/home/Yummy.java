package it.urial.de_cifris.home;

import java.io.IOException;
import java.sql.SQLException;

import it.urial.de_cifris.control.StageManager;
import it.urial.de_cifris.control.file.FileManager;
import it.urial.de_cifris.model.FileConfiguration;
import it.urial.de_cifris.model.MySQL;
import it.urial.de_cifris.view.DtoStage;
import it.urial.de_cifris.view.PrimaryStage;
import it.urial.de_cifris.view.SetupStage;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 
 * @author Federico Lattanzi Urial.it
 *
 */
public class Yummy extends Application {
	
	public static MySQL mysql;
	
	public static void main(String[] args) throws IOException {
		FileManager.init();
		
		try {
			mysql = new MySQL();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		new Thread(()->{
			
			try {
				if(mysql != null) {
					mysql.establish();
					
					mysql.execute("CREATE TABLE IF NOT EXISTS prodotto (id_prodotto INT AUTO_INCREMENT, nome VARCHAR(25), quantita INT, prezzo INT, descrizione VARCHAR(100), tipo VARCHAR(20), peso INT, provenienza VARCHAR(20), PRIMARY KEY(id_prodotto));");
					mysql.execute("CREATE TABLE IF NOT EXISTS dipendente (id_dipendente INT AUTO_INCREMENT, nome VARCHAR(25), cognome VARCHAR(25), codice_fiscale VARCHAR(16), PRIMARY KEY(id_dipendente));");
					mysql.execute("CREATE TABLE IF NOT EXISTS vende (id_vendita INT AUTO_INCREMENT, id_prodotto INT NOT NULL, id_dipendente INT NOT NULL, data DATE, PRIMARY KEY(id_vendita, id_prodotto, id_dipendente), FOREIGN KEY(id_prodotto) REFERENCES prodotto(id_prodotto), FOREIGN KEY(id_dipendente) REFERENCES dipendente(id_dipendente));");
					
					if(!mysql.exists("SELECT id_dipendente FROM dipendente WHERE id_dipendente='1';"))
						mysql.insertEmployee("Admin", "", "0000000000000000");
					
				}
			} catch (ClassNotFoundException | SQLException e) {
				try {
					FileConfiguration fc = FileManager.getConfigurationFile();
					fc.read();
					fc.write("setup", "true");
					fc.apply();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
		}).start();
		
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage = new PrimaryStage();
		StageManager.setPrimaryStage((PrimaryStage) stage);
		stage.show();
		
		SetupStage setup = new SetupStage();
		StageManager.setSetupStage(setup);
		if(setup.requiresSetup()) {
			setup.show();
		}
		
		DtoStage dto = new DtoStage();
		StageManager.setDtoStage(dto);
	}
	
	/**
	 * 
	 * @return Restituisce l'istanza di MySQL
	 */
	public static MySQL getMySQLInstance() {
		return mysql;
	}
	
}
