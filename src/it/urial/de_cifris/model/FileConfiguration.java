package it.urial.de_cifris.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class FileConfiguration {
	
	private File file;
	
	private Properties properties;
	
	public FileConfiguration(File file) {
		properties = new Properties();
		this.file = file;
	}
	
	/**
	 * 
	 * @return Restituisce tutte le propriet� del file (key=value)
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Properties getProperties() throws FileNotFoundException, IOException {
		this.read();
		return this.properties;
	}
	
	/**
	 * 
	 * @param key
	 * @return Controlla se la chiave esiste nel file
	 */
	public boolean exists(String key) {
		return properties.getProperty(key) != null;
	}
	
	/**
	 * Permette di scrivere sul file
	 * @param key
	 * @param value
	 */
	public void write(Object key, Object value) {
		properties.put(key, value);
	}
	
	/**
	 * Permettere di leggere il file e caricare le propriet� gi� presenti nel file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void read() throws FileNotFoundException, IOException {
		properties.load(new FileReader(file));
	}
	
	/**
	 * Aggiorna il file applicando tutti i cambiamenti effettuati precedentemente
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void apply() throws FileNotFoundException, IOException {
		properties.store(new PrintWriter(file), "Application created by Federico Lattanzi");
	}
	
	/**
	 * 
	 * @return Restituisce il file
	 */
	public File getFile() {
		return this.file;
	}
	
}
