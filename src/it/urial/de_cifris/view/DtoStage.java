package it.urial.de_cifris.view;

import java.io.IOException;

import it.urial.de_cifris.control.StageManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class DtoStage extends Stage {
	
	public DtoStage() throws IOException {
		AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/dto_b-1.fxml"));
		this.getIcons().add(new Image(this.getClass().getResourceAsStream("/logo.png")));
		this.setScene(new Scene(pane, 600, 400));
		this.setTitle("Aggiungi informazioni al database");
		this.setResizable(false);
		this.initModality(Modality.APPLICATION_MODAL);
	}
	
	public void defineCategory(AnchorPane productPane, AnchorPane salesPane, AnchorPane employeesPane) {
		StageManager.getDtoStage().setOnShowing(e -> {
			switch(StageManager.getPrimaryStage().getCategory()) {
			case EMPLOYEES:
				productPane.setVisible(false);
				salesPane.setVisible(false);
				employeesPane.setVisible(true);
				break;
			case HOME:
				break;
			case PRODUCTS:
				productPane.setVisible(true);
				salesPane.setVisible(false);
				employeesPane.setVisible(false);
				break;
			case SALES:
				productPane.setVisible(false);
				salesPane.setVisible(true);
				employeesPane.setVisible(false);
				break;
			
			}
		});
	}
	
}
