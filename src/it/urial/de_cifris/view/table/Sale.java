package it.urial.de_cifris.view.table;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Sale {
	
	SimpleIntegerProperty id_sale;
	SimpleIntegerProperty id_product;
	SimpleIntegerProperty id_employee;
	SimpleStringProperty date;
	
	public Sale(Integer id_sale, Integer id_product, Integer id_employee,
			String date) {
		this.id_sale = new SimpleIntegerProperty(id_sale);
		this.id_product = new SimpleIntegerProperty(id_product);
		this.id_employee = new SimpleIntegerProperty(id_employee);
		this.date = new SimpleStringProperty(date);
	}

	public SimpleIntegerProperty getId_sale() {
		return id_sale;
	}

	public SimpleIntegerProperty getId_product() {
		return id_product;
	}

	public SimpleIntegerProperty getId_employee() {
		return id_employee;
	}

	public SimpleStringProperty getDate() {
		return date;
	}
	
}