package it.urial.de_cifris.control;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;

import it.urial.de_cifris.control.file.FileManager;
import it.urial.de_cifris.home.Yummy;
import it.urial.de_cifris.model.FileConfiguration;
import it.urial.de_cifris.model.MySQL;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

public class SetupController implements Initializable {
	
	@FXML
	private TextField hostnameTextField;
	
	@FXML
	private TextField portTextField;
	
	@FXML
	private TextField databaseTextField;
	
	@FXML
	private TextField usernameTextField;
	
	@FXML
	private TextField passwordTextField;
	
	@FXML
	private JFXButton submitButton;
	
	@FXML
	private JFXSpinner statusSpinner;
	
	@FXML
	private Label statusLabel;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		portTextField.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	portTextField.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    }
		});
		
		submitButton.setOnMouseClicked(ev ->{
			Yummy.mysql = new MySQL(hostnameTextField.getText(),
					databaseTextField.getText(),
					usernameTextField.getText(),
					passwordTextField.getText(),
					Integer.parseInt(portTextField.getText()));
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					Platform.runLater( () -> { statusLabel.setText("Connessione in corso..."); });
					statusLabel.setVisible(true);
					statusSpinner.setVisible(true);
					submitButton.setDisable(true);
					try {
						Yummy.mysql.establish();

						Yummy.mysql.execute("CREATE TABLE IF NOT EXISTS prodotto (id_prodotto INT AUTO_INCREMENT, nome VARCHAR(25), quantita INT, prezzo INT, descrizione VARCHAR(100), tipo VARCHAR(20), peso INT, provenienza VARCHAR(20), PRIMARY KEY(id_prodotto));");
						Yummy.mysql.execute("CREATE TABLE IF NOT EXISTS dipendente (id_dipendente INT AUTO_INCREMENT, nome VARCHAR(25), cognome VARCHAR(25), codice_fiscale VARCHAR(16), PRIMARY KEY(id_dipendente));");
						Yummy.mysql.execute("CREATE TABLE IF NOT EXISTS vende (id_vendita INT AUTO_INCREMENT, id_prodotto INT NOT NULL, id_dipendente INT NOT NULL, data DATE, PRIMARY KEY(id_vendita, id_prodotto, id_dipendente), FOREIGN KEY(id_prodotto) REFERENCES prodotto(id_prodotto), FOREIGN KEY(id_dipendente) REFERENCES dipendente(id_dipendente));");
						
						if(!Yummy.mysql.exists("SELECT id_dipendente FROM dipendente WHERE id_dipendente='1';"))
							Yummy.mysql.insertEmployee("Admin", "", "0000000000000000");
						
						Platform.runLater(() -> {
							statusLabel.setText("Connessione riuscita");
							
							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Information Dialog");
							alert.setHeaderText(null);
							alert.setContentText("Connessione eseguita con successo.\nRiavviare il programma per utilizzarlo.");
							alert.setOnCloseRequest(e -> {
								Platform.exit();
							});
							alert.showAndWait();
							
							try {
								FileConfiguration fc = FileManager.getConfigurationFile();
								fc.read();
								fc.write("setup", "false");
								fc.write("mysql.hostname", hostnameTextField.getText());
								fc.write("mysql.port", portTextField.getText());
								fc.write("mysql.database", databaseTextField.getText());
								fc.write("mysql.username", usernameTextField.getText());
								fc.write("mysql.password", passwordTextField.getText());
								fc.apply();
							} catch (IOException e) {
								e.printStackTrace();
							}
						});
					} catch (ClassNotFoundException | SQLException e) {
						Platform.runLater( () -> {
							submitButton.setDisable(false);
							statusLabel.setText("Connessione fallita");
							statusLabel.setTooltip(new Tooltip(e.getMessage()));
						});
						try {
							Yummy.mysql.getConnection().close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						e.printStackTrace();
					}
				}
			}).start();
		});
	}

}
