package it.urial.de_cifris.control;

import it.urial.de_cifris.view.DtoStage;
import it.urial.de_cifris.view.PrimaryStage;
import it.urial.de_cifris.view.SetupStage;

public class StageManager {
	
	private static PrimaryStage primaryStage;
	private static SetupStage setupStage;
	private static DtoStage dtoStage;
	
	/**
	 * 
	 * @return Restituisce lo stage principale
	 */
	public static PrimaryStage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
	 * 
	 * Configura lo stage principale
	 */
	public static void setPrimaryStage(PrimaryStage primaryStage) {
		StageManager.primaryStage = primaryStage;
	}
	
	/**
	 * 
	 * @return Restituisce lo stage del setup
	 */
	public static SetupStage getSetupStage() {
		return setupStage;
	}
	
	/**
	 * 
	 * Configura lo stage del setup
	 */
	public static void setSetupStage(SetupStage setupStage) {
		StageManager.setupStage = setupStage;
	}
	
	/**
	 * 
	 * @return Restituisce lo stage per lo scambio di dati
	 */
	public static DtoStage getDtoStage() {
		return dtoStage;
	}

	
	/**
	 * 
	 * Configura lo stage per lo scambio di dati
	 */
	public static void setDtoStage(DtoStage dtoStage) {
		StageManager.dtoStage = dtoStage;
	}
}
