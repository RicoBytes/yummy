package it.urial.de_cifris.view;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PrimaryStage extends Stage {
	
	private Category current = Category.HOME;
	
	public PrimaryStage() throws IOException {
		AnchorPane pane = FXMLLoader.load(this.getClass().getResource("/main_b-3.fxml"));
		this.getIcons().add(new Image(this.getClass().getResourceAsStream("/logo.png")));
		this.setScene(new Scene(pane, 990, 590));
		this.setTitle("Yummy! Database application");
		this.setResizable(false);
		this.show();
		
		this.setOnCloseRequest(e -> {
			Platform.exit();
		});
	}
	
	public void setCurrentCategory(Category category) {
		this.current = category;
	}
	
	public Category getCategory() {
		return this.current;
	}
	
}
