package it.urial.de_cifris.control;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import it.urial.de_cifris.home.Yummy;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class DtoController implements Initializable {
	
	/*
	 * ROOTS COMPONENTS
	 */
	@FXML
	private AnchorPane productPane;
	@FXML
	private AnchorPane salesPane;
	@FXML
	private AnchorPane employeesPane;
	
	/*
	 * PRODUCTS COMPONENTS
	 */
	
	@FXML
	private JFXTextField nameProductTextField;
	@FXML
	private JFXTextField amountProductTextField;
	@FXML
	private JFXTextField priceProductTextField;
	@FXML
	private JFXTextArea descriptionProductTextField;
	@FXML
	private JFXTextField typeProductTextField;
	@FXML
	private JFXTextField weightProductTextField;
	@FXML
	private JFXTextField provenanceProductTextField;
	@FXML
	private JFXButton addProductButton;
	
	/*
	 * SALES COMPONENTS
	 */
	
	@FXML
	private JFXComboBox<String> productComboBox;
	
	@FXML
	private JFXComboBox<String> employeeComboBox;
	
	@FXML
	private JFXDatePicker datePicker;
	
	@FXML
	private JFXButton addSaleButton;
	
	private HashMap<Integer, Integer> cacheProduct = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> cacheEmployee = new HashMap<Integer, Integer>();
	
	/*
	 * EMPLOYEES COMPONENTS
	 */
	
	@FXML
	private JFXTextField nameEmployeeTextField;
	
	@FXML
	private JFXTextField surnameEmployeeTextField;
	
	@FXML
	private JFXTextField fiscalecodeEmployeeTextField;
	
	@FXML
	private JFXButton addEmployeeButton;
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		/*
		 * Aggiunto un ritardo (delay) compatibile con JavaFX di 1 secondo.
		 * Aggiunge i componenti alle combobox disponibili
		 * 
		 */
		Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
			
			StageManager.getDtoStage().defineCategory(productPane, salesPane, employeesPane);
			
			StageManager.getDtoStage().setOnShown(e -> {
				try {
					List<String> products = new ArrayList<>();
					Yummy.getMySQLInstance().getProducts("", "").forEach(k->{
						cacheProduct.put(products.size(), k.getId().getValue());
						products.add(k.getName().getValue() + " (ID:" + k.getId().getValue() + ")");
					});
					ObservableList<String> prodobslist = FXCollections.observableList(products);
					productComboBox.getItems().setAll(prodobslist);
					
					List<String> employees = new ArrayList<>();
					Yummy.getMySQLInstance().getEmployees("", "").forEach(k->{
						cacheEmployee.put(employees.size(), k.getId().getValue());
						employees.add(k.getName().getValue() + " (ID:" + k.getId().getValue() + ")");
					});
					ObservableList<String> emplobslist = FXCollections.observableList(employees);
					employeeComboBox.getItems().setAll(emplobslist);
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			});
		}));
		tl.play();
		
		/*
		 * Eventi legati ad ogni componente di questo stage
		 */
		addEmployeeButton.setOnMouseClicked(e -> {
			if(!(nameEmployeeTextField.getText().isEmpty()
					&& surnameEmployeeTextField.getText().isEmpty()
					&& fiscalecodeEmployeeTextField.getText().isEmpty())) {
				
				try {
					Yummy.getMySQLInstance().insertEmployee(nameEmployeeTextField.getText(),
							surnameEmployeeTextField.getText(),
							fiscalecodeEmployeeTextField.getText());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				nameEmployeeTextField.setText("");surnameEmployeeTextField.setText("");fiscalecodeEmployeeTextField.setText("");
				
				StageManager.getDtoStage().setTitle("Azione effettuata con successo!");
			}
		});
		
		addSaleButton.setOnMouseClicked(e->{
			if(productComboBox.getSelectionModel().getSelectedItem() != null
					&& employeeComboBox.getSelectionModel().getSelectedItem() != null
					&& datePicker.getValue() != null) {
				
				try {
					Yummy.getMySQLInstance().insertSale(cacheProduct.get(productComboBox.getSelectionModel().getSelectedIndex()),
							cacheEmployee.get(employeeComboBox.getSelectionModel().getSelectedIndex()),
							Date.valueOf(datePicker.getValue()));
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				productComboBox.getSelectionModel().clearSelection();
				employeeComboBox.getSelectionModel().clearSelection();
				datePicker.setValue(null);
				
				StageManager.getDtoStage().setTitle("Azione effettuata con successo!");
			}
		});
		
		addProductButton.setOnMouseClicked(e ->{
			if(!(nameProductTextField.getText().isEmpty()
					&& amountProductTextField.getText().isEmpty()
					&& priceProductTextField.getText().isEmpty()
					&& descriptionProductTextField.getText().isEmpty()
					&& typeProductTextField.getText().isEmpty()
					&& weightProductTextField.getText().isEmpty()
					&& provenanceProductTextField.getText().isEmpty())) {
				
				try {
					Yummy.getMySQLInstance().insertProduct(nameProductTextField.getText(),
							Integer.parseInt(amountProductTextField.getText()),
							Integer.parseInt(priceProductTextField.getText()),
							descriptionProductTextField.getText(),
							typeProductTextField.getText(),
							Integer.parseInt(weightProductTextField.getText()),
							provenanceProductTextField.getText());
				} catch (NumberFormatException | SQLException e1) {
					e1.printStackTrace();
				}
				
				nameProductTextField.setText("");amountProductTextField.setText("");priceProductTextField.setText("");
				descriptionProductTextField.setText("");typeProductTextField.setText("");weightProductTextField.setText("");
				provenanceProductTextField.setText("");
				
				StageManager.getDtoStage().setTitle("Azione effettuata con successo!");
				
			}
		});
		
		amountProductTextField.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				amountProductTextField.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
		
		priceProductTextField.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				priceProductTextField.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
		
		weightProductTextField.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				weightProductTextField.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
		
		fiscalecodeEmployeeTextField.textProperty().addListener((ChangeListener<String>) (ov, oldValue, newValue) -> {
		    if (fiscalecodeEmployeeTextField.getText().length() > 16) {
		        String s = fiscalecodeEmployeeTextField.getText().substring(0, 16);
		        fiscalecodeEmployeeTextField.setText(s);
		    }
		});
		
		
		
	}
	
}
